//缓存多媒体元素 （Image)
 public async static void CacheImage(Uri Source, string FileName) 
        { 
            try 
            { 
                StorageFolder MediaFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync("Media", CreationCollisionOption.OpenIfExists); 
                 
                  
                  
                StorageFile destinationFile = await MediaFolder.CreateFileAsync( 
                    FileName, CreationCollisionOption.GenerateUniqueName); 
 
                BackgroundDownloader downloader = new BackgroundDownloader(); 
                DownloadOperation download = downloader.CreateDownload(Source, destinationFile); 
                download.StartAsync(); 
 
            } 
            catch (Exception ex) 
            { 
 
            } 
        }