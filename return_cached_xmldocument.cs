//返回缓存的xmldocument
public async static Task<XmlDocument> GetCacheXML(string Name) 
        { 
            ///This Method return the Offline Cache Content of the specific file Name  
            /// 
            try 
            { 
                var file = await ApplicationData.Current.LocalFolder.GetFileAsync(Name); 
 
                var Content = await XmlDocument.LoadFromFileAsync(file); 
 
 
                return Content; 
            } 
            catch (Exception) 
            { 
                return null; 
            } 
        }