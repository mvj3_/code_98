//这个方法缓存 XMLDocument  ，修改本地的多媒体路径
 foreach (var MediaElement in Content.GetElementsByTagName(MediaElementTag)) 
            { 
                StorageFolder MediaFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync("Media", CreationCollisionOption.OpenIfExists); 
 
 
                var FileName = MediaElement.Attributes[0].InnerText.Split('/').Last(); 
 
                CacheImage(new Uri(MediaElement.Attributes[0].InnerText), FileName); 
 
                MediaElement.InnerText = "ms-appdata:///local/Media/" + FileName; 
            } 
            var file = await ApplicationData.Current.LocalFolder.CreateFileAsync(Name, CreationCollisionOption.ReplaceExisting); 
            var data = Content.GetXml(); 
            await FileIO.WriteTextAsync(file, data);