//这个方法缓存xml文件、多媒体元素（图片、音视频等）
try 
            { 
                
                var file = await ApplicationData.Current.LocalFolder.CreateFileAsync(Name, CreationCollisionOption.ReplaceExisting); 
                await FileIO.WriteTextAsync(file, Content.GetXml()); 
            } 
            catch (Exception ex) 
            { throw ex; }